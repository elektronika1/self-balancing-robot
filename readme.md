# Self balancing robot using FreeRTOS on ESP32

This project uses Real Time Operating System (RTOS) on ESP32 to demonstrate the
usage of FreeRTOS for self balancing two wheeled robot

## Briefing

Self-balancing robots in essence, is an inverted pendulum on wheels, a derivative
of the inverted pendulum on a cart. Unlike traditional robots, which are in a 
constant state of equilibrium, this robot is a naturally unstable system. Its 
design is more complex, as it needs to be actively controlled to maintain its 
upright position, however, it benefits from being able to turn on the spot. It 
will be prevented from falling by giving acceleration to the wheels according to
its inclination from the vertical. If the bot gets tilts by an angle, then in the
frame of the wheels, the Centre of mass of the bot will experience a pseudo force
which will apply a torque opposite to the direction of tilt . To maintain the
robot upright, the most commonly used controllers are Proportional-Integral-Derivative (PID)
and is perhaps the most used controller. The algorithm is described by the following
equation:  

``` math
\mu(t) = K_pe(t)+K_i\int e(t)dt + K_d \frac{de(t)}{dt} 
```

Where $`e(t)`$ is difference between Target angle and current angle of the robot.
Where, $`\mu(t)`$ is the output of the controller  which is the error that needs
to be corrected and $`K_p, K_i \ \& K_d`$ are the tuning parameters . It is relatively
easy to implement and does not require a model of the system. Tuning of the parameters
is done with trial and error, where this robot used $`K_p =100; K_d =0.30 \ and \ K_i=140  `$  
Tilt angle estimation is performed using a combination of a gyroscope and an
accelerometer are used. A Filter is necessary since gyroscopes have a bias and 
accelerometers are relatively noisy. To combine the measurements, Kalman Filter 
technique is used.  

## Setup

 ESP32 Microcontroller even though has two cores, single core is used on FreeRTOS
 where the operation is divided into two tasks.  

1. Task that Reads IMU values and passes through Kalman Filtering to get Tilt and
estimated PID Value.  
2. Task that converts PID Value to the Motor Speed Control and Applies to the Motor
by applying necessary Directions.  
3. There is an additional Task to have a serial console control of parameters 
like PID parameters and targeted Tilt angle.

Task 3 is suspended after Tuning and order of the priority is **Task3<Task1 <Task2**

**Priority Inheritance Mechanism** is invoked by using Mutex mechanism, wherein
**Task2** and **Task1** needs a Mutex to execute, which causes each of the task
not to corrupt the Global Data variables where the PID values are stored.  It is
to be noted that the Arduino Framework by default uses FreeRTOS based structure 
where setup() is one time run task with Priority number of 12, loop() is repetitive
task with priority number of 1, which we called as **Task3**. **Task1** and
**Task2** has priority number of 2 and 3 respectively which are called with
frequency delays of 10 and 25 milliseconds respectively.  

The Arduino Sketch is provided without forward or backward motion which could be
incorporated by adjusting the Target angle that makes robot trying to achieve 
which would result in a directed motion. Since the robot was balanced using a 
single geared dc motor with double shaft, left or right control wasn’t available
but that shouldn’t hamper the performance of RTOS on working with Self Balancing
Robot.  

##### Hardware used: 

- ESP32 DevkitC
- LM293D Motor Driver
- MPU6050 IMU Sensor
- Geared 5v DC Motor

##### Software used:

- freeRTOS on ESP32 IDF
- Arduino IDE Framework 

## Working

High-level schematic and its build is shown below. 

<img src="./readme_support/working_schematic.jpg" alt="Schematic" width="480" height="480" style="zoom:50%;" />

Power Supply for the L293D with 5v ~ 12v is allowed which is expected to be DC 
with least ripple factor and a capacitor backed bank to avoid spikes when Motors
try to balance quickly while at equilibrium.  ESP32 and MPU6050 is powered 
separately from the USB Mini Cable, since noise needs to be avoided at every 
possible point to avoid erroneous readings. MPU6050 is accessed via I2C Connection.  

<img src="./readme_support/triplet.jpg" alt="RTOSbot" style="zoom:50%;" />

If the angle of tilt goes beyond 40° the motors are commanded to stop to avoid, 
until an external force tries to bring it to tilt less than 40°. A dead band was
tried to create but it resulted in "Limited Stability". Initially the Sensor is 
calibrated for 100 readings before a reading is considered to be valid. The 
consecutive readings are passed through Kalman filtering to obtain a tilt. Based
on tilt, a PID Value is produced which is translated to Motor Speed variable that
decides the PWM of motor with which it should run. The closer to target angle
lesser the speed.  

![Robot stages](./readme_support/combined.gif)

The whole experiment was to solve a timing critical task, like the inverted
pendulum classic problem using the RTOS. More operations can be added to the list
using an RTOS ensuring a smooth operation

## Authors

* **Babu P S** - [eflaner](https://gitlab.com/eflaner) 
* **Ajish Zacharias**

## License

This project uses Hardware licensed under the [CERN Open Hardware Licence v2](http://www.opensource.org/licenses/mit-license.php) and the firmware with [MIT License](http://www.opensource.org/licenses/mit-license.php).

## Acknowledgements

This project uses various libraries from the esp32 to achieve the objective. 

Image Credits: *Shamsudheen, Jithin & Jinesh* 

Indebted to people who provided suggestions for improvement and made this possible.