#include <Kalman.h>
#include <Wire.h>

// use first channel of 16 channels (started from zero)
#define MOTOR_CHANNEL_0     0
#define LED_INBUILT         2
#define MPU6050_SLAVE_ADDRESS  0x68
#define GYROSCALEFACTOR 131
#define PWM_OUTPUT_MAX 255
#define PWM_OUTPUT_MIN 190
#define PID_OUTPUT_MIN 0
#define PID_OUTPUT_MAX 950
#define SLOPE 1.0*(PWM_OUTPUT_MAX-PWM_OUTPUT_MIN)/(PID_OUTPUT_MAX-PID_OUTPUT_MIN)
// use 13 bit precission for LEDC timer
#define PWM_TIMER_13_BIT  8


// use 5000 Hz as a LEDC base frequency
#define PWM_BASE_FREQ     5000
#define RIGT_EN             13
#define RIGT_IN_0           27
#define RIGT_IN_1           14
#define SCL  22
#define SDA  21
// MPU6050 few configuration register addresses
#define MPU6050_REGISTER_SMPLRT_DIV     0x19
#define MPU6050_REGISTER_USER_CTRL      0x6A
#define MPU6050_REGISTER_PWR_MGMT_1     0x6B
#define MPU6050_REGISTER_PWR_MGMT_2     0x6C
#define MPU6050_REGISTER_CONFIG         0x1A
#define MPU6050_REGISTER_GYRO_CONFIG    0x1B
#define MPU6050_REGISTER_ACCEL_CONFIG   0x1C
#define MPU6050_REGISTER_FIFO_EN        0x23
#define MPU6050_REGISTER_INT_ENABLE     0x38
#define MPU6050_REGISTER_ACCEL_XOUT_H   0x3B
#define MPU6050_REGISTER_SIGNAL_PATH_RESET   0x68

#define LEAST_PRIORITY_TASK 2
#define RETRY_ANGLE_TASK  20
#define RETRY_MOTOR_SPEED_TASK 20
#define __min(a,b) ((a)<(b)?(a):(b))

int16_t AccelX, AccelY, AccelZ, Temperature, GyroX, GyroY, GyroZ;
int MotorSpeed = 200;
uint32_t timer, errorTimer;
float Kp =  100;
float Kd =  -0.30;
float Ki =  140;
float targetAngle = 1.50;
float errorSum, prevError;
double dt,kalAngleX, kalAngleY;
Kalman kalmanX;
Kalman kalmanY;
SemaphoreHandle_t xTaskSemaphore = NULL;
const TickType_t xFrequency = 25/portTICK_PERIOD_MS; //Call every 25ms
const TickType_t xFrequency_GA = 10/portTICK_PERIOD_MS; //Call every 10ms

void setup() {
  errorSum = 0; prevError = 0;
  Serial.begin(115200);
  Wire.begin(SDA, SCL); // Initialize I2C
  MPU6050_Init();
  Serial.print(".");
  delay(1000);
  calibrateSensors();
  double roll  = atan2(AccelY, AccelZ) * RAD_TO_DEG;
  double pitch = atan(-AccelX / sqrt(AccelY * AccelY + AccelZ * AccelZ)) * RAD_TO_DEG;
  kalmanX.setAngle(roll); // Set starting angle
  kalmanY.setAngle(pitch);
  pinMode(RIGT_IN_0, OUTPUT); pinMode(RIGT_IN_1, OUTPUT);
  pinMode(RIGT_EN, OUTPUT); pinMode(LED_INBUILT, OUTPUT);
  ledcSetup(MOTOR_CHANNEL_0, PWM_BASE_FREQ, PWM_TIMER_13_BIT);
  ledcAttachPin(RIGT_EN, MOTOR_CHANNEL_0);
  ledcWrite(MOTOR_CHANNEL_0, MotorSpeed);
  StopMotor();
  xTaskSemaphore = xSemaphoreCreateMutex();
  if (( xTaskSemaphore)!= NULL )  xSemaphoreGive((xTaskSemaphore));
  xTaskCreate(
    getAngleTask,             /* Task function. */
    "Get Tilt angle",         /* name of task. */
    30000,                    /* Stack size of task */
    NULL,                     /* parameter of the task */
    LEAST_PRIORITY_TASK,      /* priority of the task */
    NULL);
  xTaskCreate(
    controlSpeed,             /* Task function. */
    "Control Motor Speed",    /* name of task. */
    30000,                    /* Stack size of task */
    NULL,                     /* parameter of the task */
    LEAST_PRIORITY_TASK+1,    /* priority of the task */
    NULL);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available()) {
    // read the most recent byte (which will be from 0 to 255):
    char inByte = Serial.read();
    if (inByte == 'w') moveForward();
    if (inByte == 's') moveReverse();
    //    if (inByte == 'a') moveLeft();
    //    if (inByte == 'd') moveRight();
    if (inByte == 'h') MotorSpeed = MotorSpeed + 20;
    if (inByte == 'l') MotorSpeed =  MotorSpeed - 20;
    if (inByte == 'p') Kp = Kp + 5;
    if (inByte == 'o') Ki = Ki + 10;
    if (inByte == 'i') Kd = Kd + 0.05;
    if (inByte == 'm') Kp = Kp - 5;
    if (inByte == 'n') Ki = Ki - 10;
    if (inByte == 'b') Kd = Kd - 0.05;
    if (inByte == 't') targetAngle = targetAngle  + 0.5;
    if (inByte == 'v') targetAngle = targetAngle  - 0.5;
    if (inByte == 'q')  vTaskSuspend( NULL );
    if (inByte == 'y') {
      Serial.print("\r\n TargetAngle: "); Serial.print(targetAngle);
      Serial.print("\r\n Kp: "); Serial.print(Kp);
      Serial.print("\r\n Ki: "); Serial.print(Ki);
      Serial.print("\r\n Kd: "); Serial.println(Kd);
    }
  }
}
void getAngleTask( void * parameter )
{
  for (;;) {
    if (xSemaphoreTake( xTaskSemaphore, (TickType_t) RETRY_ANGLE_TASK ) == pdTRUE){
      double Gy;
      Read_RawValue(MPU6050_SLAVE_ADDRESS, MPU6050_REGISTER_ACCEL_XOUT_H);
      dt = (double)(micros() - timer) / 1000000;
      timer = micros();
      double roll  = atan2(AccelY, AccelZ) * RAD_TO_DEG;
      double pitch = atan(-AccelX / sqrt(AccelY * AccelY + AccelZ * AccelZ)) * RAD_TO_DEG;
      Gy = (double)GyroY / GYROSCALEFACTOR;
      if ((roll < -90 && kalAngleX > 90) || (roll > 90 && kalAngleX < -90)) {
        kalmanX.setAngle(roll);
        kalAngleX = roll;
      } else
        kalAngleX = kalmanX.getAngle(roll, ((double)GyroX / GYROSCALEFACTOR), dt); // Calculate the angle using a Kalman filter
      if (abs(kalAngleX) > 90)
        Gy = -Gy; // Invert rate, so it fits the restriced accelerometer reading
      kalAngleY = kalmanY.getAngle(pitch, Gy, dt);
      returnPIDValue(kalAngleY - targetAngle);
      Serial.println("getAngleTask");
      Serial.print(kalAngleX);Serial.print(" ");Serial.println(kalAngleY);
      xSemaphoreGive(xTaskSemaphore);
      vTaskDelay(xFrequency_GA);
    }
  }
  vTaskDelete( NULL );
}
void controlSpeed( void * parameter ) {
  for (;;) {
    if ( xSemaphoreTake( xTaskSemaphore, (TickType_t) RETRY_MOTOR_SPEED_TASK) == pdTRUE ){
      digitalWrite(LED_INBUILT, HIGH);
      Serial.print("current Angle: "); Serial.print(kalAngleY);
      float balanceByError = returnPIDValue(kalAngleY - targetAngle);
      Serial.print("  PID: "); Serial.print(balanceByError);
      Serial.print("  PWM: "); Serial.println(MotorSpeed);
      balancing(balanceByError);
      Serial.println("controlSpeed");
      xSemaphoreGive( xTaskSemaphore );
      vTaskDelay(xFrequency);
      digitalWrite(LED_INBUILT, LOW);
    }
  }
  vTaskDelete( NULL );
}
void moveForward() {
  digitalWrite(RIGT_IN_0, LOW);
  digitalWrite(RIGT_IN_1, HIGH);
}
void moveReverse() {
  digitalWrite(RIGT_IN_0, HIGH);
  digitalWrite(RIGT_IN_1, LOW);
}
void StopMotor() {
  digitalWrite(RIGT_IN_0, LOW);
  digitalWrite(RIGT_IN_1, LOW);
}
void I2C_Write(uint8_t deviceAddress, uint8_t regAddress, uint8_t data) {
  Wire.beginTransmission(deviceAddress);
  Wire.write(regAddress);
  Wire.write(data);
  Wire.endTransmission();
}
void MPU6050_Init() {
  delay(150);
  I2C_Write(MPU6050_SLAVE_ADDRESS, MPU6050_REGISTER_SMPLRT_DIV, 0x07);
  I2C_Write(MPU6050_SLAVE_ADDRESS, MPU6050_REGISTER_PWR_MGMT_1, 0x01);
  I2C_Write(MPU6050_SLAVE_ADDRESS, MPU6050_REGISTER_PWR_MGMT_2, 0x00);
  I2C_Write(MPU6050_SLAVE_ADDRESS, MPU6050_REGISTER_CONFIG, 0x00);
  I2C_Write(MPU6050_SLAVE_ADDRESS, MPU6050_REGISTER_GYRO_CONFIG, 0x00);//set +/-250 degree/second full scale
  I2C_Write(MPU6050_SLAVE_ADDRESS, MPU6050_REGISTER_ACCEL_CONFIG, 0x00);// set +/- 2g full scale
  I2C_Write(MPU6050_SLAVE_ADDRESS, MPU6050_REGISTER_FIFO_EN, 0x00);
  I2C_Write(MPU6050_SLAVE_ADDRESS, MPU6050_REGISTER_INT_ENABLE, 0x01);
  I2C_Write(MPU6050_SLAVE_ADDRESS, MPU6050_REGISTER_SIGNAL_PATH_RESET, 0x00);
  I2C_Write(MPU6050_SLAVE_ADDRESS, MPU6050_REGISTER_USER_CTRL, 0x00);
}
// read all 14 register
void Read_RawValue(uint8_t deviceAddress, uint8_t regAddress) {
  Wire.beginTransmission(deviceAddress);
  Wire.write(regAddress);
  Wire.endTransmission();
  Wire.requestFrom(deviceAddress, (uint8_t)14);
  AccelX = (((int16_t)Wire.read() << 8) | Wire.read());
  AccelY = (((int16_t)Wire.read() << 8) | Wire.read());
  AccelZ = (((int16_t)Wire.read() << 8) | Wire.read());
  Temperature = (((int16_t)Wire.read() << 8) | Wire.read());
  GyroX = (((int16_t)Wire.read() << 8) | Wire.read());
  GyroY = (((int16_t)Wire.read() << 8) | Wire.read());
  GyroZ = (((int16_t)Wire.read() << 8) | Wire.read());
}
void calibrateSensors() {
  digitalWrite(LED_INBUILT, HIGH);
  int64_t zeroValues[3];
  for (uint8_t i = 0; i < 100; i++) { // Take the average of 100 readings
    Read_RawValue(MPU6050_SLAVE_ADDRESS, MPU6050_REGISTER_ACCEL_XOUT_H);
    zeroValues[0] += AccelY;
    zeroValues[1] += AccelZ;
    zeroValues[2] += AccelX;
    delay(10);
  }
  zeroValues[0] /= 100; // Accelerometer Y-axis
  zeroValues[1] /= 100; // Accelerometer Z-axis
  zeroValues[2] /= 100; // Accelerometer X-axis
  AccelY = zeroValues[0];
  AccelZ = zeroValues[1];
  AccelX = zeroValues[2];
  digitalWrite(LED_INBUILT, LOW);
}
void SpeedControl() {
  MotorSpeed = (MotorSpeed < PWM_OUTPUT_MIN) ? PWM_OUTPUT_MIN : ((MotorSpeed > PWM_OUTPUT_MAX) ? PWM_OUTPUT_MAX : MotorSpeed);
  ledcWrite(MOTOR_CHANNEL_0, MotorSpeed);
}
void balancing(int balanceByError) {
  if ((kalAngleY > 40) || (kalAngleY < -40)) { // Out of balance
    StopMotor();
    Serial.println("N0");
  }// else if ((kalAngleY > 0)&&(kalAngleY < 4) || (kalAngleY > -4)&&(kalAngleY < 0)) { // Inside Balance
//    StopMotor();
//    Serial.println("Yes");
//  }
  else if (balanceByError > 0) {
    MotorSpeed = MapValues(abs(balanceByError));
    SpeedControl();
    moveReverse();
  } else if (balanceByError < 0) {
    MotorSpeed = MapValues(abs(balanceByError));
    SpeedControl();
    moveForward();
  }
}
int MapValues(float input) {
  return PWM_OUTPUT_MIN + floor (((double)SLOPE) * (abs(input) - PID_OUTPUT_MIN) + 0.5);
}
float returnPIDValue(float error) {
  float propTerm = Kp * (error);
  errorSum +=  Ki * error;
  float IntTerm = errorSum;
  float diffTerm = Kd * ((error - prevError) / (micros() - errorTimer));
  prevError = error;
  errorTimer = micros();
  //  errorSum = errorSum+error;
  errorSum = constrain(errorSum, -300, 300);
  //  return propTerm - diffTerm;
  //  return (Kp * (error)) + (Ki * (errorSum) * dt) - (Kd * (kalAngleX - prevKalAngleX) / dt);
  return propTerm + IntTerm * dt - diffTerm;
}
